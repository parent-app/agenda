package com.openclassrooms.parentapp.agenda.fixture;

import com.openclassrooms.parentapp.agenda.dto.AppointmentDto;
import com.openclassrooms.parentapp.agenda.entity.Appointment;
import com.openclassrooms.parentapp.agenda.entity.EAppointmentType;

import java.time.LocalDateTime;
import java.util.List;

public class AppointmentFixture {

    public static Appointment buildAppointment() {
        return Appointment.builder()
                .description("description")
                .startDate(LocalDateTime.now())
                .endDate(LocalDateTime.now().plusHours(1))
                .type(EAppointmentType.VACCINE)
                .userId("abc1")
                .build();
    }

    public static AppointmentDto buildAppointmentDto() {
        return AppointmentDto.builder()
                .description("description")
                .startDate(LocalDateTime.now())
                .endDate(LocalDateTime.now().plusHours(1))
                .type(EAppointmentType.VACCINE)
                .userId("abc1")
                .build();
    }

    public static List<AppointmentDto> buildListAppointmentDto() {
        AppointmentDto appointment1 = AppointmentDto.builder()
                .description("description")
                .startDate(LocalDateTime.now())
                .endDate(LocalDateTime.now().plusHours(1))
                .type(EAppointmentType.VACCINE)
                .userId("abc1")
                .build();
        AppointmentDto appointment2 = AppointmentDto.builder()
                .description("description2")
                .startDate(LocalDateTime.now())
                .endDate(LocalDateTime.now().plusHours(2))
                .type(EAppointmentType.DOCTOR)
                .userId("abc1")
                .build();

        return List.of(appointment1, appointment2);
    }

    public static List<Appointment> builListAppointment() {
        Appointment appointment1 = Appointment.builder()
                .description("description")
                .startDate(LocalDateTime.now().plusMinutes(1))
                .endDate(LocalDateTime.now().plusHours(1))
                .type(EAppointmentType.VACCINE)
                .userId("abc1")
                .build();
        Appointment appointment2 = Appointment.builder()
                .description("description2")
                .startDate(LocalDateTime.now().plusMinutes(1))
                .endDate(LocalDateTime.now().plusHours(2))
                .type(EAppointmentType.DOCTOR)
                .userId("abc1")
                .build();
        Appointment appointment3 = Appointment.builder()
                .description("description3")
                .startDate(LocalDateTime.now().plusDays(1))
                .type(EAppointmentType.OTHER)
                .userId("abc2")
                .build();

        return List.of(appointment1, appointment2, appointment3);
    }

    /*public static List<AppointmentLightDto> buildListAppointmentLightDto() {
        AppointmentLightDto appointmentLightDto1 = AppointmentLightDto.builder()
                .id(1L)
                .startDate(LocalDateTime.now())
                .endDate(LocalDateTime.now().plusHours(1))
                .type(EAppointmentType.VACCINE)
                .userId(1L)
                .build();

        AppointmentLightDto appointmentLightDto2 = AppointmentLightDto.builder()
                .id(2L)
                .startDate(LocalDateTime.now())
                .endDate(LocalDateTime.now().plusHours(2))
                .type(EAppointmentType.DOCTOR)
                .userId(1L)
                .build();

        return List.of(appointmentLightDto1, appointmentLightDto2);
    }*/
}

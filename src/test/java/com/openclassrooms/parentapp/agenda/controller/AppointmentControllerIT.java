package com.openclassrooms.parentapp.agenda.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.openclassrooms.parentapp.agenda.dto.AppointmentDto;
import com.openclassrooms.parentapp.agenda.dto.AppointmentLightDto;
import com.openclassrooms.parentapp.agenda.entity.Appointment;
import com.openclassrooms.parentapp.agenda.entity.EAppointmentType;
import com.openclassrooms.parentapp.agenda.fixture.AppointmentFixture;
import com.openclassrooms.parentapp.agenda.repository.AppointmentRepository;
import com.openclassrooms.parentapp.agenda.repository.ReminderRepository;
import com.openclassrooms.parentapp.agenda.service.AppointmentService;
import com.openclassrooms.parentapp.agenda.service.ReminderService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class AppointmentControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AppointmentService appointmentService;

    @Autowired
    private AppointmentRepository appointmentRepository;

    @Autowired
    private ReminderService reminderService;

    @Autowired
    private ReminderRepository reminderRepository;

    @Autowired
    private ObjectMapper mapper;

    @BeforeEach
    @AfterEach
    public void init() {
        reminderRepository.deleteAll();
        appointmentRepository.deleteAll();
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    void getUserAppointments() throws Exception {
        // GIVEN
        appointmentRepository.saveAll(AppointmentFixture.builListAppointment());

        // WHEN
        mockMvc.perform(get(AppointmentController.ROOT_RESOURCE).param("userId", "abc1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].userId", is("abc1")))
                .andExpect(jsonPath("$.[0].type", is("VACCINE")))
                .andExpect(jsonPath("$.[1].userId", is("abc1")))
                .andExpect(jsonPath("$.[1].type", is("DOCTOR")))
                .andReturn();
    }

    @Test
    @WithUserDetails
    void getUserAppointments_forbidden() throws Exception {
        // WHEN
        mockMvc.perform(get(AppointmentController.ROOT_RESOURCE).param("userId", "abc1"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    void getUserNextAppointments() throws Exception {
        // GIVEN
        List<Appointment> appointments = new ArrayList<>(AppointmentFixture.builListAppointment());
        appointments.add(
                Appointment.builder()
                        .type(EAppointmentType.OTHER)
                        .userId("abc1")
                        .startDate(LocalDateTime.now().minusDays(1))
                        .endDate(LocalDateTime.now().minusDays(1).plusHours(1))
                        .build());
        appointmentRepository.saveAll(appointments);

        // WHEN
        mockMvc.perform(get(AppointmentController.ROOT_RESOURCE + AppointmentController.NEXT_APPOINTMENTS_RESOURCE)
                .param("userId", "abc1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    @WithUserDetails
    void getUserNextAppointments_forbidden() throws Exception {
        // WHEN
        mockMvc.perform(get(AppointmentController.ROOT_RESOURCE + AppointmentController.NEXT_APPOINTMENTS_RESOURCE)
                .param("userId", "abc1"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    void getAppointmentById() throws Exception {
        // GIVEN
        List<Appointment> appointmentList = (List<Appointment>) appointmentRepository.saveAll(AppointmentFixture.builListAppointment());

        // WHEN
        String resultString = mockMvc.perform(get(AppointmentController.ROOT_RESOURCE + "/" + appointmentList.get(2).getId()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        // THEN
        assertThat(mapper.readValue(resultString, AppointmentLightDto.class)).usingRecursiveComparison().ignoringFields("description", "startDate").isEqualTo(appointmentList.get(2));
    }

    @Test
    @WithUserDetails
    void getAppointmentsById_forbidden() throws Exception {
        // GIVEN
        List<Appointment> appointmentList = (List<Appointment>) appointmentRepository.saveAll(AppointmentFixture.builListAppointment());

        // WHEN
        mockMvc.perform(get(AppointmentController.ROOT_RESOURCE + "/" + appointmentList.get(2).getId()))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "USER")
    void createAppointment() throws Exception {
        // GIVEN
        AppointmentDto appointmentDto = AppointmentFixture.buildAppointmentDto();

        // WHEN
        mockMvc.perform(
                post(AppointmentController.ROOT_RESOURCE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(appointmentDto)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.userId", is("abc1")))
                .andExpect(jsonPath("$.type", is("VACCINE")))
                .andReturn();
    }

    @Test
    @WithMockUser(roles = "VIEWER")
    void createAppointment_forbidden() throws Exception {
        // GIVEN
        AppointmentDto appointmentDto = AppointmentFixture.buildAppointmentDto();

        // WHEN
        mockMvc.perform(
                post(AppointmentController.ROOT_RESOURCE)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(appointmentDto)))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    void updateAppointment() throws Exception {
        // GIVEN
        Appointment appointment = appointmentRepository.save(AppointmentFixture.buildAppointment());
        AppointmentDto appointmentDto = AppointmentFixture.buildAppointmentDto();
        appointmentDto.setId(appointment.getId());
        appointmentDto.setType(EAppointmentType.COMPULSORY_MEDICAL_EXAMINATION);

        // WHEN
        mockMvc.perform(
                put(AppointmentController.ROOT_RESOURCE + "/" + appointmentDto.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(appointmentDto)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(appointmentDto.getId().intValue())))
                .andExpect(jsonPath("$.type", is("COMPULSORY_MEDICAL_EXAMINATION")))
                .andReturn();
    }

    @Test
    @WithUserDetails
    void updateAppointment_forbidden() throws Exception {
        // GIVEN
        Appointment appointment = appointmentRepository.save(AppointmentFixture.buildAppointment());
        AppointmentDto appointmentDto = AppointmentFixture.buildAppointmentDto();
        appointmentDto.setId(appointment.getId());
        appointmentDto.setType(EAppointmentType.COMPULSORY_MEDICAL_EXAMINATION);

        // WHEN
        mockMvc.perform(
                put(AppointmentController.ROOT_RESOURCE + "/" + appointmentDto.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(appointmentDto)))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    void deleteAppointment() throws Exception {
        // GIVEN
        Appointment appointment = appointmentRepository.save(AppointmentFixture.buildAppointment());

        // WHEN
        mockMvc.perform(delete(AppointmentController.ROOT_RESOURCE + "/" + appointment.getId()))
                .andExpect(status().isNoContent())
                .andReturn();
    }

    @Test
    @WithMockUser(roles = "VIEWER")
    void deleteAppointment_forbidden() throws Exception {
        // GIVEN
        Appointment appointment = appointmentRepository.save(AppointmentFixture.buildAppointment());

        // WHEN
        mockMvc.perform(delete(AppointmentController.ROOT_RESOURCE + "/" + appointment.getId()))
                .andExpect(status().isForbidden());
    }

    // TODO : supprimer si rappels fixes
    /*
    @Test
    public void createReminder() throws Exception {
        // GIVEN
        Appointment appointment = appointmentRepository.save(AppointmentFixture.buildAppointment());
        ReminderDto reminderDto = ReminderDto.builder()
                .date(LocalDateTime.of(2021, 3, 1, 10, 35, 5))
                .appointmentId(appointment.getId())
                .build();

        // WHEN
        mockMvc.perform(
                post(AppointmentController.ROOT_RESOURCE + "/" + appointment.getId() + AppointmentController.REMINDER_RESOURCE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(reminderDto)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.date", is(reminderDto.getDate().toString())))
                .andExpect(jsonPath("$.appointmentId", is(appointment.getId().intValue())))
                .andReturn();
    }

    @Test
    public void updateReminder() throws Exception {
        // GIVEN
        Appointment appointment = appointmentRepository.save(AppointmentFixture.buildAppointment());
        ReminderDto reminderDto = ReminderDto.builder()
                .date(LocalDateTime.of(2021, 3, 1, 10, 35, 5))
                .appointmentId(appointment.getId())
                .build();
        ReminderDto reminderDtoSaved = reminderService.create(reminderDto);
        reminderDtoSaved.setDate(LocalDateTime.of(2021, 3, 1, 12, 10, 5));


        // WHEN
        mockMvc.perform(
                put(AppointmentController.ROOT_RESOURCE + "/" + appointment.getId() + AppointmentController.REMINDER_RESOURCE)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(reminderDtoSaved)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.date", is(reminderDtoSaved.getDate().toString())))
                .andExpect(jsonPath("$.appointmentId", is(appointment.getId().intValue())))
                .andReturn();
    }

    @Test
    public void deleteReminder() throws Exception {
        // GIVEN
        Appointment appointment = appointmentRepository.save(AppointmentFixture.buildAppointment());
        ReminderDto reminderDto = ReminderDto.builder()
                .date(LocalDateTime.of(2021, 3, 1, 10, 35, 5))
                .appointmentId(appointment.getId())
                .build();
        ReminderDto reminderSaved = reminderService.create(reminderDto);

        // WHEN
        mockMvc.perform(
                delete(AppointmentController.ROOT_RESOURCE + "/" + appointment.getId() + AppointmentController.REMINDER_RESOURCE + "/" + reminderSaved.getId()))
                .andExpect(status().isNoContent())
                .andReturn();
    }*/
}

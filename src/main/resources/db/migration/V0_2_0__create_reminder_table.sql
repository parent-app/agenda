CREATE TABLE t_reminder
(
    id BIGSERIAL PRIMARY KEY,
    date TIMESTAMP NOT NULL,
    appointment_id BIGSERIAL,
    CONSTRAINT FK_REMINDER_APPOINTMENT
        FOREIGN KEY (appointment_id)
            REFERENCES t_appointment(id)
);

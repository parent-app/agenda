CREATE TABLE t_appointment
(
    id BIGSERIAL PRIMARY KEY,
    start_date TIMESTAMP NOT NULL,
    end_date TIMESTAMP,
    description VARCHAR(255),
    type VARCHAR(40),
    user_id VARCHAR(250)
);

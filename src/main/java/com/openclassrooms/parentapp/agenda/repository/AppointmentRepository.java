package com.openclassrooms.parentapp.agenda.repository;

import com.openclassrooms.parentapp.agenda.entity.Appointment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface AppointmentRepository extends CrudRepository<Appointment, Long> {

    List<Appointment> findAllByUserId(String userId);

    List<Appointment> findAllByUserIdAndStartDateBetween(String userId, LocalDateTime startDate, LocalDateTime endDate);
}

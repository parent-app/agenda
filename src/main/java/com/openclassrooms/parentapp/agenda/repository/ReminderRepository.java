package com.openclassrooms.parentapp.agenda.repository;

import com.openclassrooms.parentapp.agenda.entity.Reminder;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReminderRepository extends CrudRepository<Reminder, Long> { // TODO : supprimer si on utilise les rappels fixes

    List<Reminder> findAllByAppointmentId(Long appointmentId);
}

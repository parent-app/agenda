package com.openclassrooms.parentapp.agenda.controller;

import com.openclassrooms.parentapp.agenda.dto.AppointmentDto;
import com.openclassrooms.parentapp.agenda.service.AppointmentService;
import com.openclassrooms.parentapp.agenda.service.ReminderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(AppointmentController.ROOT_RESOURCE)
@RequiredArgsConstructor
@Tag(name = "appointments", description = "Controller to handle appointments requests")
@CrossOrigin(origins = "*")
public class AppointmentController {

    public static final String ROOT_RESOURCE = "/api/appointments";
    public static final String NEXT_APPOINTMENTS_RESOURCE = "/next";
    public static final String REMINDER_RESOURCE = "/reminders";
    private final AppointmentService appointmentService;
    private final ReminderService reminderService;

    @GetMapping
    @Operation(summary = "Get all the appointments of a user")
    @PreAuthorize("hasRole('ADMIN') or #userId == authentication.principal.id")
    public List<AppointmentDto> getUserAppointments(@Parameter(name = "userId", description = "Id of the user for whom we want to retrieve all his appointments")
                                                             @RequestParam String userId) {
        return appointmentService.findAllByUserId(userId);
    }

    @GetMapping(NEXT_APPOINTMENTS_RESOURCE)
    @Operation(summary = "Get the user's next appointments")
    @PreAuthorize("hasRole('ADMIN') or #userId == authentication.principal.id")
    public List<AppointmentDto> getUserNextAppointments(@Parameter(name = "userId", description = "Id of the user for whom we want to retrieve all his appointments")
                                                            @RequestParam String userId) {
        return appointmentService.findUserNextAppointments(userId);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get an appointment by id")
    @PostAuthorize("hasRole('ADMIN') or returnObject.userId == authentication.principal.id")
    public AppointmentDto getAppointmentById(@Parameter(name = "id", description = "Id of the requested appointment")
                                                 @PathVariable Long id) {
        return appointmentService.findById(id);
    }

    @PostMapping
    @Operation(summary = "Create an appointment")
    @RolesAllowed({"ADMIN", "USER"})
    public AppointmentDto createAppointment(@RequestBody @Valid AppointmentDto appointmentDto) {
        return appointmentService.create(appointmentDto);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Update an appointment")
    @PreAuthorize("hasRole('ADMIN') or #appointmentDto.userId == authentication.principal.id")
    public AppointmentDto updateAppointment(@Parameter(name = "id", description = "Appointment id") Long id,
                                            @RequestBody @Valid AppointmentDto appointmentDto) {
        return appointmentService.update(appointmentDto);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete an appointment")
    @RolesAllowed({"ADMIN", "USER"})
    public ResponseEntity<Void> deleteAppointment(@Parameter(name = "id", description = "Appointment id")
                                                      @PathVariable Long id) {
        appointmentService.delete(id);
        return ResponseEntity.noContent().build();
    }

    // TODO : supprimer si on estime qu'on aura un rappel fixe pour chaque évènement
    /*
    @PostMapping("/{id}" + REMINDER_RESOURCE)
    @Operation(summary = "Create a reminder")
    public ReminderDto createReminder(@Parameter(name = "id", description = "Appointment id") @PathVariable Long id,
                                         @RequestBody @Valid ReminderDto reminderDto) {
        return reminderService.create(reminderDto);
    }

    @PutMapping("/{id}" + REMINDER_RESOURCE)
    @Operation(summary = "Update reminder")
    public ReminderDto updateReminder(@Parameter(name = "id", description = "Appointment id") @PathVariable Long id,
                                      @RequestBody @Valid ReminderDto reminderDto) {
        return reminderService.update(reminderDto);
    }

    @DeleteMapping("/{id}" + REMINDER_RESOURCE + "/{reminderId}")
    @Operation(summary = "Delete reminder")
    public ResponseEntity<Void> deleteReminder(@Parameter(name = "id", description = "Appointment id") @PathVariable Long id,
                                               @Parameter(name = "reminderId", description = "Reminder id") @PathVariable Long reminderId) {
        reminderService.delete(reminderId);
        return ResponseEntity.noContent().build();

    }*/
}

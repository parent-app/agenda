package com.openclassrooms.parentapp.agenda.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {

    private Long id;
    private String externalId;
    private String username;
    private String email;
    private String password;
    private ERole role;
}

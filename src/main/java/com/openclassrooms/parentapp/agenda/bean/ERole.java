package com.openclassrooms.parentapp.agenda.bean;

public enum ERole {
    ROLE_ADMIN,
    ROLE_USER
}

package com.openclassrooms.parentapp.agenda.dto;

import com.openclassrooms.parentapp.agenda.entity.EAppointmentType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AppointmentDto {

    private Long id;

    @NotNull
    private LocalDateTime startDate;

    private LocalDateTime endDate;

    private String description;

    @NotNull
    private EAppointmentType type;

    @NotNull
    private String userId;

   /* private List<ReminderDto> reminders;*/ // TODO : supprimer si on utilise les rappels fixes
}

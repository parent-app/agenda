package com.openclassrooms.parentapp.agenda.dto;

import com.openclassrooms.parentapp.agenda.entity.EAppointmentType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AppointmentLightDto { // TODO : supprimer si on utilise les rappels fixes

    private Long id;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private EAppointmentType type;
    private String userId;
}

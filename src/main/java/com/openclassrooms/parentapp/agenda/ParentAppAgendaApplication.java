package com.openclassrooms.parentapp.agenda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParentAppAgendaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ParentAppAgendaApplication.class, args);
	}

}

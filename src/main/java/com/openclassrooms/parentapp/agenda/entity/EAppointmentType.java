package com.openclassrooms.parentapp.agenda.entity;

public enum EAppointmentType {
    DOCTOR,
    VACCINE,
    PHARMACY,
    PEDIATRIC_NURSE,
    COMPULSORY_MEDICAL_EXAMINATION,
    OTHER

}

package com.openclassrooms.parentapp.agenda.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "t_appointment",
        schema = "agenda")
public class Appointment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "start_date")
    @NotNull
    private LocalDateTime startDate;

    @Column(name = "end_date")
    private LocalDateTime endDate;

    private String description;

    @Enumerated(value = EnumType.STRING)
    @NotNull
    private EAppointmentType type;

    @Column(name = "user_id")
    @NotNull
    private String userId;

    // TODO : supprimer si on estime qu'on aura un rappel fixe pour chaque évènement
    /*
    @OneToMany(mappedBy = "appointment", fetch = FetchType.LAZY)
    private List<Reminder> reminders;*/
}

package com.openclassrooms.parentapp.agenda.service;

import com.openclassrooms.parentapp.agenda.dto.ReminderDto;
import com.openclassrooms.parentapp.agenda.entity.Appointment;
import com.openclassrooms.parentapp.agenda.entity.Reminder;
import com.openclassrooms.parentapp.agenda.mapper.ReminderMapper;
import com.openclassrooms.parentapp.agenda.repository.AppointmentRepository;
import com.openclassrooms.parentapp.agenda.repository.ReminderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ReminderService {

    private final ReminderRepository reminderRepository;
    private final AppointmentRepository appointmentRepository;
    private final ReminderMapper reminderMapper;

    public ReminderDto create(ReminderDto reminderDto) {
        Appointment appointment = appointmentRepository.findById(reminderDto.getAppointmentId()).orElseThrow(EntityNotFoundException::new);
        Reminder reminder = Reminder.builder()
                .date(reminderDto.getDate())
                .appointment(appointment)
                .build();
        return reminderMapper.toReminderDto(reminderRepository.save(reminder));
    }

    public ReminderDto update(ReminderDto reminderDto) {
        Reminder reminder = reminderRepository.findById(reminderDto.getId()).orElseThrow(EntityNotFoundException::new);
        reminder.setDate(reminderDto.getDate());

        return reminderMapper.toReminderDto(reminderRepository.save(reminder));
    }

    public void delete(Long id) {
        Reminder reminder = reminderRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        reminderRepository.delete(reminder);
    }

    public void deleteAllByAppointmentId(Long appointmentId) {
        List<Reminder> reminders = reminderRepository.findAllByAppointmentId(appointmentId);
        reminderRepository.deleteAll(reminders);
    }
}

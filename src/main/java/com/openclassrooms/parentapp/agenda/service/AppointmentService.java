package com.openclassrooms.parentapp.agenda.service;

import com.openclassrooms.parentapp.agenda.dto.AppointmentDto;
import com.openclassrooms.parentapp.agenda.entity.Appointment;
import com.openclassrooms.parentapp.agenda.mapper.AppointmentMapper;
import com.openclassrooms.parentapp.agenda.repository.AppointmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AppointmentService {

    private final AppointmentRepository appointmentRepository;
    private final AppointmentMapper appointmentMapper;
    private final ReminderService reminderService;

    public List<AppointmentDto> findAllByUserId(String userId) {
        return appointmentMapper.toListAppointmentDto(appointmentRepository.findAllByUserId(userId));
    }

    public List<AppointmentDto> findUserNextAppointments(String userId) {
        var startDate = LocalDateTime.now().withHour(0).withMinute(0);
        var endDate = startDate.plusDays(7).withHour(23).withMinute(59);
        return appointmentMapper.toListAppointmentDto(appointmentRepository.findAllByUserIdAndStartDateBetween(userId, startDate, endDate));
    }

    public AppointmentDto findById(Long id) {
        return appointmentMapper.toAppointmentDto(
                        appointmentRepository.findById(id)
                                .orElseThrow(EntityNotFoundException::new));
    }

    public AppointmentDto create(AppointmentDto appointmentDto) {
        return appointmentMapper.toAppointmentDto(
                appointmentRepository.save(
                        appointmentMapper.toAppointment(appointmentDto)));
    }

    public AppointmentDto update(AppointmentDto appointmentDto) {
        Appointment appointment = appointmentRepository
                .findById(appointmentDto.getId()).orElseThrow(EntityNotFoundException::new);
        appointment.setDescription(appointmentDto.getDescription());
        appointment.setStartDate(appointmentDto.getStartDate());
        appointment.setEndDate(appointmentDto.getEndDate());
        appointment.setType(appointmentDto.getType());

        return appointmentMapper.toAppointmentDto(appointmentRepository.save(appointment));
    }

    public void delete(Long id) {
        reminderService.deleteAllByAppointmentId(id);
        Appointment appointment = appointmentRepository
                .findById(id).orElseThrow(EntityNotFoundException::new);
        appointmentRepository.delete(appointment);
    }
}

package com.openclassrooms.parentapp.agenda.security;

import com.openclassrooms.parentapp.agenda.bean.ERole;
import com.openclassrooms.parentapp.agenda.bean.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return UserDetailsImpl.build(User.builder()
                .externalId("externalId")
                .id(0L)
                .username("username")
                .email("email")
                .password("password")
                .role(ERole.ROLE_USER)
                .build());
    }
}

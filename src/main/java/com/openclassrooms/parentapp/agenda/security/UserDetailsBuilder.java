package com.openclassrooms.parentapp.agenda.security;

import com.openclassrooms.parentapp.agenda.bean.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsBuilder {

    @Autowired
    private JwtUtils jwtUtils;

    public UserDetails build(String jwt)  {
        return UserDetailsImpl.build(User.builder()
                .externalId(jwtUtils.getExternalIdFromJwtToken(jwt))
                .id(null)
                .username(null)
                .email(null)
                .password(null)
                .role(jwtUtils.getRoleFromJwtToken(jwt))
                .build());
    }
}

package com.openclassrooms.parentapp.agenda.security;

import com.openclassrooms.parentapp.agenda.bean.ERole;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * JWT utility class to validate JWT, and get username from JWT
 */
@Slf4j
@Component
public class JwtUtils {

    @Value("${app.jwtSecret}")
    private String jwtSecret;

    /**
     * To get user's external id from JWT token
     *
     * @param token JWT token
     * @return externalId
     */
    public String getExternalIdFromJwtToken(String token) {
        return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().get("extId").toString();
    }

    public ERole getRoleFromJwtToken(String token) {
        return ERole.valueOf(Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().get("role").toString());
    }

    /**
     * Method to validate JWT token
     *
     * @param token token to validate
     * @return boolean
     */
    public boolean validateJwtToken(String token) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token);
            return true;
        } catch (MalformedJwtException e) {
            log.error("JWT token invalide : {}", e.getMessage());
        } catch (ExpiredJwtException e) {
            log.error("JWT token expiré : {}", e.getMessage());
        } catch (UnsupportedJwtException e) {
            log.error("JWT token non supporté : {}", e.getMessage());
        } catch (IllegalArgumentException e) {
            log.error("JWT token vide : {}", e.getMessage());
        }
        return false;
    }
}

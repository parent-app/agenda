package com.openclassrooms.parentapp.agenda.mapper;

import com.openclassrooms.parentapp.agenda.dto.ReminderDto;
import com.openclassrooms.parentapp.agenda.entity.Reminder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ReminderMapper { // TODO : supprimer si on utilise les rappels fixes
    @Mapping(target = "appointmentId", source = "appointment.id")
    ReminderDto toReminderDto(Reminder reminder);
    Reminder toReminder(ReminderDto reminderDto);
}

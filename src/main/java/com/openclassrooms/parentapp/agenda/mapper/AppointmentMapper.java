package com.openclassrooms.parentapp.agenda.mapper;

import com.openclassrooms.parentapp.agenda.dto.AppointmentDto;
import com.openclassrooms.parentapp.agenda.dto.AppointmentLightDto;
import com.openclassrooms.parentapp.agenda.entity.Appointment;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = ReminderMapper.class)
public interface AppointmentMapper {

    Appointment toAppointment(AppointmentDto appointmentDto);
    AppointmentDto toAppointmentDto(Appointment appointment);
    List<AppointmentLightDto> toListAppointmentLightDto(List<Appointment> appointmentList); // TODO : supprimer si on utilise pas le AppointmentLightDto
    List<AppointmentDto> toListAppointmentDto(List<Appointment> appointmentList);
}
